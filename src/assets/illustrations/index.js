import Back from './back.jpg';
import Facebook from './facebook.png';
import Gitlab from './gitlab.png';
import Instagram from './instagram.png';
import Login from './login.png';
import Photo from './photo.png';
import Register from './register.svg';
import Twitter from './twitter.png';
import Welcome from './welcome.jpg';

export {
  Back,
  Facebook,
  Gitlab,
  Instagram,
  Login,
  Photo,
  Register,
  Twitter,
  Welcome,
};
