import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Register, BackIcon} from '../../../assets';
import {Input, Button} from '../../../components';
import {colors} from '../../../utils/colors';
import {useSelector, useDispatch} from 'react-redux';
import {setForm} from '../../../redux';

const RegisterPage = ({navigation}) => {
  //setup akhir dan mengaktifkan redux di Page
  //dengan memanggil reducer yang digunakan
  // bisa menjadi const {form} jika yang form saja yg d pakai dari reducer nya
  const RegisterReducer = useSelector((state) => state.RegisterReducer);
  //untuk merubah isi redux

  const dispatch = useDispatch();

  //replace oleh redux
  // standar membuat state d fungsional component
  // form, nama state, setForm, utk ubah value state
  // const [form, setForm] = useState({
  //   fullName: '',
  //   email: '',
  //   password: '',
  // });

  //componentdidmount
  // useEffect(() => {
  //   console.log('reducer', RegisterReducer);
  // }, [RegisterReducer]);

  const sendData = () => {
    console.log('data yang dikirim', RegisterReducer.form);
    //disiapkan untuk rest API.
    // axios.post('url', this.state);
  };

  // replace dg redux
  // const onInputChange = (value, input) => {
  //   setForm({
  //     ...form, //harus ada, kalau gak ada, d ganti jadi fullName saja
  //     [input]: value,
  //   });
  //   // setForm({[input]: value});
  // };

  const onInputChange = (inputType, value) => {
    // dispatch({type: 'SET_TITLE'}); //bisa namanya bebas, tapi standarnya gini, ini semisal kita mau ganti title
    //memanggil action reducer
    // dispatch({type: 'SET_FORM', inputType: inputType, inputValue: value});
    // perintahnya d pindahkan ke action.js
    dispatch(setForm(inputType, value));
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}> */}
        <View style={styles.buttonBack}>
          {/* Component SVG */}
          {/* <BackIcon /> */}
          <Button type="icon" name="back" onPress={() => navigation.goBack()} />
          {/* <Image source={Back} /> */}
        </View>
        {/* </TouchableOpacity> */}
        <View style={styles.imageSet}>
          {/* <Image source={Register} /> */}
          <Register />
          <Text style={styles.registerText}>
            Silahkan lengkapi data anda untuk daftar {RegisterReducer.title}
          </Text>
        </View>
        <Input
          placeholder="Nama Lengkap"
          value={RegisterReducer.form.fullName}
          onChangeText={(value) => {
            onInputChange('fullName', value);
          }}
        />
        <Input
          placeholder="Email"
          value={RegisterReducer.form.email}
          onChangeText={(value) => {
            onInputChange('email', value);
          }}
        />
        <Input
          placeholder="Password"
          value={RegisterReducer.form.password}
          onChangeText={(value) => {
            onInputChange('password', value);
          }}
          secureTextEntry={true}
        />
        <Input placeholder="Konfirmasi Password" secureTextEntry={true} />
        <View style={{alignItems: 'center'}}>
          <Button title="DAFTAR" onPress={sendData} />
        </View>
      </ScrollView>
    </View>
  );
};

export default RegisterPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  buttonBack: {
    marginTop: 20,
    marginLeft: 15,
  },
  imageSet: {
    alignItems: 'center',
  },
  registerText: {
    marginTop: 36,
    color: colors.default,
    marginLeft: 64,
    marginRight: 64,
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 40,
  },
});
