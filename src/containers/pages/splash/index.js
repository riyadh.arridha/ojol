import React, {Component} from 'react';
import {View, Text} from 'react-native';

export default class Splash extends Component {
  componentDidMount() {
    return setTimeout(() => {
      this.props.navigation.replace('Welcome');
    }, 2000);
  }
  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text>Splash Screen</Text>
      </View>
    );
  }
}
