import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Register} from '../../../assets';
import {Input, Button} from '../../../components';
import {colors} from '../../../utils/colors';
import {useSelector, useDispatch} from 'react-redux';
import {setForm} from '../../../redux';

const LoginPage = ({navigation}) => {
  //setup akhir dan mengaktifkan redux di Page
  //dengan memanggil reducer yang digunakan
  // bisa menjadi const {form} jika yang form saja yg d pakai dari reducer nya
  const {form} = useSelector((state) => state.LoginReducer);
  //untuk merubah isi redux

  const dispatch = useDispatch();

  //replace oleh redux
  // standar membuat state d fungsional component
  // form, nama state, setForm, utk ubah value state
  // const [form, setForm] = useState({
  //   fullName: '',
  //   email: '',
  //   password: '',
  // });

  //componentdidmount
  // useEffect(() => {
  //   console.log('reducer', RegisterReducer);
  // }, [RegisterReducer]);

  const sendData = () => {
    console.log('data yang dikirim', form);
    //disiapkan untuk rest API.
    // axios.post('url', this.state);
  };

  // replace dg redux
  // const onInputChange = (value, input) => {
  //   setForm({
  //     ...form, //harus ada, kalau gak ada, d ganti jadi fullName saja
  //     [input]: value,
  //   });
  //   // setForm({[input]: value});
  // };

  const onInputChange = (inputType, value) => {
    // dispatch({type: 'SET_TITLE'}); //bisa namanya bebas, tapi standarnya gini, ini semisal kita mau ganti title
    //memanggil action reducer
    // dispatch({type: 'SET_FORM', inputType: inputType, inputValue: value});
    // perintahnya d pindahkan ke action.js
    dispatch(setForm(inputType, value));
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}> */}
        <View style={styles.buttonBack}>
          {/* Component SVG */}
          <Button type="icon" name="back" onPress={() => navigation.goBack()} />
          {/* <Image source={Back} /> */}
        </View>
        {/* </TouchableOpacity> */}
        <View style={styles.imageSet}>
          {/* <Image source={Register} /> */}
          <Register />
          <Text style={styles.registerText}>
            Silahkan masukkan email dan password anda
          </Text>
        </View>

        <Input
          placeholder="Email"
          value={form.email}
          onChangeText={(value) => {
            onInputChange('email', value);
          }}
        />
        <Input
          placeholder="Password"
          value={form.password}
          onChangeText={(value) => {
            onInputChange('password', value);
          }}
          secureTextEntry={true}
        />

        <View style={{alignItems: 'center'}}>
          <Button title="LOGIN" onPress={sendData} />
        </View>
      </ScrollView>
    </View>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  buttonBack: {
    marginTop: 20,
    marginLeft: 15,
  },
  imageSet: {
    alignItems: 'center',
  },
  registerText: {
    marginTop: 36,
    color: colors.default,
    marginLeft: 64,
    marginRight: 64,
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 40,
  },
});
