import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {Button} from '../../../components/atoms';

const ButtonAction = ({desc, title, onPress}) => {
  return (
    <View style={{alignItems: 'center'}}>
      <View>
        <Text style={styles.loginDesc}>{desc}</Text>
      </View>
      <Button title={title} onPress={onPress} />
    </View>
  );
};

export default ButtonAction;

const styles = StyleSheet.create({
  loginDesc: {
    fontSize: 12,
    color: '#BFBFBF',
    textAlign: 'center',
    paddingHorizontal: 60,
  },
});
