import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import ButtonAction from './buttonAction';
// import {Welcome} from '../../../assets';
import {colors} from '../../../utils/colors';
import {Welcome} from '../../../assets/';

const WelcomeAuth = ({navigation}) => {
  const handleGoTo = (screen) => {
    navigation.navigate(screen);
  };
  return (
    <View style={styles.container}>
      <View style={styles.image}>
        {/* Component SVG */}
        {/* bisa memiliki atribut height dan width dan bisa d berikan style */}
        {/* <Welcome /> */}
        <Image source={Welcome} />
      </View>
      <View>
        <Text style={styles.welcomeText}>Selamat Datang di SkilPro</Text>
      </View>
      <ButtonAction
        desc="Silahkan pilih masuk, jika anda sudah memiliki akun"
        title="MASUK"
        onPress={() => handleGoTo('Login')}
      />
      <ButtonAction
        desc="Atau pilih daftar, jika anda belum memiliki akun"
        title="DAFTAR"
        onPress={() => handleGoTo('Register')}
      />
    </View>
  );
};

export default WelcomeAuth;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 186,
    height: 139,
  },
  welcomeText: {
    fontSize: 24,
    color: colors.default,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 11,
    marginBottom: 50,
  },
  loginDesc: {
    fontSize: 12,
    color: '#BFBFBF',
    textAlign: 'center',
    paddingHorizontal: 60,
  },
});
