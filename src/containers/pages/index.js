import Splash from './splash';
import Login from './login';
import Welcome from './welcome';
import Register from './register';

export {Splash, Login, Welcome, Register};
