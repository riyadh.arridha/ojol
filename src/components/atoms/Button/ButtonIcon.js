import React from 'react';
import {BackIcon} from '../../../assets';
import {TouchableOpacity} from 'react-native-gesture-handler';

const ButtonIcon = ({...rest}) => {
  return (
    <TouchableOpacity {...rest}>
      {rest.name === 'back' && <BackIcon />}
    </TouchableOpacity>
  );
};

export default ButtonIcon;
