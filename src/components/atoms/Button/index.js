import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {colors} from '../../../utils/colors';
import ButtonIcon from './ButtonIcon';

const Button = ({title, onPress, type, name}) => {
  if (type === 'icon') {
    return <ButtonIcon name={name} onPress={onPress} />;
  }
  //props title utk teks, dan props onPress utk menekan tombol
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text style={styles.loginTitle}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  loginTitle: {
    fontSize: 12,
    color: '#FFFFFF',
    // color: "#000000",
    textAlign: 'center',
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: colors.default,
    height: 48,
    width: 232,
    marginTop: 11,
    marginBottom: 11,
    justifyContent: 'center',
    borderRadius: 232 / 2,
  },
});
