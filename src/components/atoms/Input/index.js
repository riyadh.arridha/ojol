import React from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {colors} from '../../../utils/colors';

const Input = ({placeholder, ...rest}) => {
  return (
    <View style={styles.inputBar}>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        placeholderTextColor={colors.default}
        // value={value}
        // onChangeState={}
        // teknik ini aja. d kumpulkan sisa props d ...rest
        {...rest}
      />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  input: {
    height: 40,
    borderColor: '#C4C4C4',
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    color: colors.default,
    marginBottom: 20,
    fontSize: 12,
  },
  inputBar: {
    marginLeft: 64,
    marginRight: 64,
  },
});
