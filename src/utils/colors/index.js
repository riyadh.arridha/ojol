export const colors = {
  default: '#B53471',
  disable: '#A5B1C2',
  dark: '474747',
  text: {
    default: '#FFF',
    light: '#BFBFBF',
  },
};

//tidak memakai export default biar bisa menambah komponen
// export const colorsText ={
//     default:
// }
